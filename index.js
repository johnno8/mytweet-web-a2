/**
 * Created by John on 11/10/2016.
 */
'use strict';

const Hapi = require('hapi');
const Bell = require('bell');
require('./app/models/db');

let server = new Hapi.Server();
server.connection({ port: process.env.PORT || 4000 });

server.register([require('inert'), require('vision'), require('hapi-auth-cookie'), Bell,
  {
    register: require('disinfect'),
    options: {
      disinfectQuery: true,
      disinfectParams: true,
      disinfectPayload: false,
    },
  }, ], (err) => {

  if (err) {
    throw err;
  }

  server.auth.strategy('standard', 'cookie', {
    password: 'secretpasswordnotrevealedtoanyone',
    cookie: 'mytweet-cookie',
    isSecure: false,
    ttl: 24 * 60 * 60 * 1000,
    redirectTo: '/login',
  });

  let bellAuthOptions = {
    provider: 'github',
    password: 'github-encryption-password-secure', // Password for encrypting the cookie
    clientId: 'fb47831270d9e4fe528e', //'Your App Id',
    clientSecret: 'ccbd49ea7667ffe0d388fb15d488bd7b54dbf0fb', //'Your App Secret',
    isSecure: false,
  };

  server.auth.strategy('github-oauth', 'bell', bellAuthOptions);


  server.auth.default({
    strategy: 'standard',
  });

  server.views({
    engines: {
      hbs: require('handlebars'),
    },
    relativeTo: __dirname,
    path: './app/views',
    layoutPath: './app/views/layout',
    partialsPath: './app/views/partials',
    layout: true,
    isCached: false,
  });

  server.route(require('./routes'));
  server.route(require('./routesapi'));

  server.route([{
    method: 'GET',
    path: '/loginoauth',          // Login route where access is granted
    config: {
      auth: 'github-oauth',

      handler: function (request, reply) {
        if (request.auth.isAuthenticated) {
          request.cookieAuth.set(request.auth.credentials);
          return reply('Hello ' + request.auth.credentials.profile.displayName);
        }

        reply('Not logged in...').code(401);
      },
    },
  }, {
    method: 'GET',
    path: '/account',    // Show the full profile shared with this app
    config: {
      handler: function (request, reply) {

        reply(request.auth.credentials.profile);
      },
    },
  }, {
    method: 'GET',
    path: '/landing',        // landing page after callback
    config: {
      auth: {
        mode: 'optional',
      },
      handler: function (request, reply) {

        if (request.auth.isAuthenticated) {
          return reply('welcome back ' + request.auth.credentials.profile.displayName);
        }

        reply();
      },
    },
  }, {
    method: 'GET',
    path: '/logoutoauth',    // clear auth cookie
    config: {
      auth: false,
      handler: function (request, reply) {

        request.cookieAuth.clear();
        reply.redirect('/');
      },
    },
  },
  ]);

  server.start((err) => {
    if (err) {
      throw err;
    }

    console.log('Server listening at:', server.info.uri);
  });

});

