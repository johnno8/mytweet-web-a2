/**
 * Created by John on 11/10/2016.
 */
const Accounts = require('./app/controllers/accounts');
const Assets = require('./app/controllers/assets');
const Tweets = require('./app/controllers/tweets');
const Users = require('./app/controllers/users');
const Administrators = require('./app/controllers/administrators');
const Following = require('./app/controllers/following');

module.exports = [

  { method: 'GET', path: '/', config: Accounts.main },
  { method: 'GET', path: '/signup', config: Accounts.signup },
  { method: 'POST', path: '/register', config: Accounts.register },
  { method: 'GET', path: '/login', config: Accounts.login },
  { method: 'POST', path: '/login', config: Accounts.authenticate },
  { method: 'GET', path: '/settings', config: Accounts.viewSettings },
  { method: 'POST', path: '/settings', config: Accounts.updateSettings },
  { method: 'GET', path: '/logout', config: Accounts.logout },

  { method: 'GET', path: '/home', config: Tweets.home },
  { method: 'POST', path: '/postATweet', config: Tweets.postATweet },
  { method: 'GET', path: '/timeline', config: Tweets.timeline },
  { method: 'POST', path: '/delete/{id}', config: Tweets.deleteTweet },
  { method: 'GET', path: '/deleteAll', config: Tweets.deleteAll },
  { method: 'GET', path: '/getPicture/{id}', config: Tweets.getPicture },

  { method: 'GET', path: '/allusers', config: Users.allUsers },
  { method: 'POST', path: '/publictimeline/{id}', config: Users.publicTimeline },
  { method: 'POST', path: '/follow/{id}', config: Users.follow },

  { method: 'GET', path: '/following', config: Following.getFriends },
  { method: 'POST', path: '/unfollow/{id}', config: Following.unfollow },

  { method: 'GET', path: '/adminlogin', config: Administrators.adminlogin },
  { method: 'POST', path: '/adminauth', config: Administrators.authenticate },
  { method: 'GET', path: '/adminhome', config: Administrators.adminhome },
  { method: 'GET', path: '/adminregister', config: Administrators.adminRegister },
  { method: 'POST', path: '/registeruser', config: Administrators.registerUser },
  { method: 'GET', path: '/adminusers', config: Administrators.adminUsers },
  { method: 'POST', path: '/deleteuser/{id}', config: Administrators.deleteUser },
  { method: 'GET', path: '/admintimeline', config: Administrators.adminTimeline },
  { method: 'GET', path: '/getPictureAdmin/{id}', config: Administrators.getPicture },
  { method: 'POST', path: '/admindeltweet/{id}', config: Administrators.deleteTweet },
  { method: 'POST', path: '/deleteusertweets/{id}', config: Administrators.deleteUserTweets },
  { method: 'GET', path: '/adminlogout', config: Administrators.logout },

  {
    method: 'GET',
    path: '/{param*}',
    config: { auth: false },
    handler: Assets.servePublicDirectory,
  },

];
