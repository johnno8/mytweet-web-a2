/**
 * Created by John on 18/12/2016.
 */
const UsersApi = require('./app/api/usersapi');
const TweetsApi = require('./app/api/tweetsapi');

module.exports = [
  { method: 'GET', path: '/api/users', config: UsersApi.find },
  { method: 'GET', path: '/api/users/{id}', config: UsersApi.findOne },
  { method: 'POST', path: '/api/users', config: UsersApi.create },
  { method: 'DELETE', path: '/api/users/{id}', config: UsersApi.deleteOne },
  { method: 'DELETE', path: '/api/users', config: UsersApi.deleteAll },
  { method: 'GET', path: '/api/users/{email}/{password}', config: UsersApi.login },

  { method: 'GET', path: '/api/tweets', config: TweetsApi.find },
  { method: 'GET', path: '/api/users/{id}/tweets', config: TweetsApi.findTweets },
  { method: 'POST', path: '/api/users/{id}/tweets', config: TweetsApi.sendTweet },
  { method: 'DELETE', path: '/api/tweets', config: TweetsApi.deleteAllTweets },
  { method: 'DELETE', path: '/api/users/{id}/tweets', config: TweetsApi.deleteUsersTweets },
  { method: 'DELETE', path: '/api/tweets/{id}', config: TweetsApi.deleteOneTweet },
];
