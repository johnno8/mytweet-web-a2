/**
 * Created by John on 18/12/2016.
 */
'use strict'

const User = require('../models/user');
const Boom = require('boom');
const bcrypt = require('bcrypt-nodejs');

// Return all users
exports.find = {

  auth: false,

  handler: function (request, reply) {
    User.find({}).exec().then(users => {
      reply(users);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

// Return one user based on id stripped from the URL
exports.findOne = {

  auth: false,

  handler: function (request, reply) {
    User.findOne({ _id: request.params.id }).then(user => {
      if (user != null) {
        reply(user);
      }

      reply(Boom.notFound('id not found'));
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },
};

// Create a new user
exports.create = {

  auth: false,

  handler: function (request, reply) {
    const user = new User(request.payload);
    const plaintextPassword = user.password;

    bcrypt.hash(plaintextPassword, null, null, function (err, hash) {
      user.password = hash;

      return user.save().then(newUser => {
        reply(newUser).code(201);
      }).catch(err => {
        reply(Boom.badImplementation('error creating user'));
      });
    });
  },
};

// Delete one user based on id taken from the URL
exports.deleteOne = {

  auth: false,

  handler: function (request, reply) {
    User.remove({ _id: request.params.id }).then(user => {
      reply(user).code(204);
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

// Delete all users
exports.deleteAll = {

  auth: false,

  handler: function (request, reply) {
    User.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing candidates'));
    });
  },

};

// Log in a user
exports.login = {

  auth: false,

  handler: function (request, reply) {
    const email = request.params.email;
    const password = request.params.password;

    User.findOne({ email: email }).then(foundUser => {
      bcrypt.compare(password, foundUser.password, function (err, res) {
        if (res)
        {
          reply().code(204);
        }
      });
    }).catch(err => {
      reply(Boom.unauthorized('login failed'));
    });
  },
};
