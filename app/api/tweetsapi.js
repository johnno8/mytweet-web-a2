/**
 * Created by John on 19/12/2016.
 */
'use strict'

const Tweet =  require('../models/tweet');
const User = require('../models/user');
const Boom = require('boom');

// Return all tweets
exports.find = {

  auth: false,

  handler: function (request, reply) {
    Tweet.find({}).populate('tweetor').then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};

// Return all tweets for a particular user
exports.findTweets = {

  auth: false,

  handler: function (request, reply) {
    Tweet.find({ tweetor: request.params.id }).then(tweets => {
      reply(tweets);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

// Create a tweet
exports.sendTweet = {

  auth: false,

  handler: function (request, reply) {
    const tweet = new Tweet(request.payload);
    tweet.tweetor = request.params.id;
    tweet.save().then(newTweet => {
      reply(newTweet).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error sending tweet'));
    });
  },

};

// Delete all tweets
exports.deleteAllTweets = {

  auth: false,

  handler: function (request, reply) {
    Tweet.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Donations'));
    });
  },

};

// Delete all tweets for a particular user
exports.deleteUsersTweets = {
  auth: false,

  handler: function (request, reply) {
    const tweetorId = request.params.id;
    Tweet.remove({ tweetor: tweetorId }).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Donation'));
    });
  },
};

// Delete on tweet based on its id
exports.deleteOneTweet = {

  auth: false,

  handler: function (request, reply) {
    let id = request.params.id;

    Tweet.findOneAndRemove({ _id: id }).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing tweet'));
    });
  },
};
