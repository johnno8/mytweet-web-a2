/**
 * Created by John on 19/10/2016.
 */
'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  regdate: String,
  following: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },],
  followers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },],
});

const User = mongoose.model('User', userSchema);
module.exports = User;
