/**
 * Created by John on 19/10/2016.
 */
const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
  content: String,
  tweetor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  date: String,
  picture: { data: Buffer, contentType: String },
});

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;
