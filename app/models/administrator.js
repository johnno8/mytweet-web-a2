/**
 * Created by John on 02/11/2016.
 */
'use strict'

const mongoose = require('mongoose');

const administratorSchema = mongoose.Schema({
  email: String,
  password: String,
});

const Administrator = mongoose.model('Administrator', administratorSchema);
module.exports = Administrator;
