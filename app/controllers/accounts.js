/**
 * Created by John on 11/10/2016.
 */
'use strict';

const User = require('../models/user');
const Joi = require('joi');
const bcrypt = require('bcrypt-nodejs');

// Renders the main landing page
exports.main = {

  auth: false,

  handler: (request, reply) => {
    reply.view('main', { title: 'Welcome to MyTweet' });
  },

};

// Renders the user sign up page
exports.signup = {

  auth: false,

  handler: (request, reply) => {
    reply.view('signup', { title: 'Sign up to MyTweet' });
  },

};

// Renders the user login page
exports.login = {

  auth: false,

  handler: (request, reply) => {
    reply.view('login', { title: 'Login to MyTweet' });
  },

};

// Recieves user details from signup form and creates a new user
exports.register = {

  auth: false,
  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },

  handler: function (request, reply) {
    let d = new Date();
    let datestring = ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) +
        '-' + d.getFullYear();
    const user = new User(request.payload);
    const plaintextPassword = user.password;

    bcrypt.hash(plaintextPassword, null, null, function (err, hash) {
      user.password = hash;
      user.regdate = datestring;
      return user.save().then(newUser => {
        reply.redirect('login');
      }).catch(err => {
        reply.redirect('/');
      });
    });

  },

};

// Recieves user details from login form and authenticates it
exports.authenticate = {

  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Log in error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },

  handler: function (request, reply) {
    const user = request.payload;

    User.findOne({ email: user.email }).then(foundUser => {
      bcrypt.compare(user.password, foundUser.password, function (err, res) {

        if (res) {
          request.cookieAuth.set({
            loggedIn: true,
            loggedInUser: user.email,
          });
          reply.redirect('/home');
        } else {
          reply.redirect('/signup');
        }
      });
    }).catch(err => {
      reply.redirect('/');
    });

  },

};

// Renders settings page and populates the form with users existing details
exports.viewSettings = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ email: userEmail }).then(foundUser => {
      reply.view('settings', { title: 'Edit account settings', user: foundUser });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

// Recieves user details from settings form and updates the existing details in the db
exports.updateSettings = {

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('settings', {
        title: 'Edit details error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },

  },

  handler: function (request, reply) {
    const editedUser = request.payload;
    const loggedInUserEmail = request.auth.credentials.loggedInUser;

    User.findOne({ email: loggedInUserEmail }).then(user => {
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;

      bcrypt.hash(editedUser.password, null, null, function (err, hash) {
        user.password = hash;
        return user.save();
      });
    }).then(user => {
      reply.view('settings', { title: 'Edit Account Settings', user: user });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Clears cookie and logs out user
exports.logout = {

  auth: false,

  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },

};
