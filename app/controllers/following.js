/**
 * Created by John on 01/01/2017.
 */

'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');

//Renders following view populated with following and friend's tweets
exports.getFriends = {

  handler: function (request, reply) {

    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ email: userEmail }).populate('following').then(user => {

      let friendsTweets = [];
      user.following.forEach(function (friend) {
        User.findOne({ email: friend.email }).then(userFriend => {
          Tweet.find({ tweetor: userFriend }).populate('tweetor').then(tweets => {
            friendsTweets.push.apply(friendsTweets, tweets);
          });
        });//closing then(userFriend
      });//closing forEach

      reply.view('following', {
        title: 'Following',
        friends: user.following,
        tweets: friendsTweets,
      }).catch(err => {
        reply.redirect('/timeline');
      });
    });
  },
};

//Unfollow a user i.e. removes that user from following list
exports.unfollow = {

  handler: function (request, reply) {
    let id = request.params.id;
    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ _id: id }).then(friend => {
      User.findOne({ email: userEmail }).then(foundUser => {
        if (foundUser.following.indexOf(friend._id) > -1) {
          foundUser.following.splice(foundUser.following.indexOf(friend._id), 1);
          foundUser.save();
        }

        if (friend.followers.indexOf(foundUser._id) > -1) {
          friend.followers.splice(friend.followers.indexOf(foundUser._id), 1);
          friend.save();
        }

      }).then(foundUser => {
        reply.redirect('/following');
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};
