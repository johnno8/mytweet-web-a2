/**
 * Created by John on 02/11/2016.
 */
'use strict';

const Administrator = require('../models/administrator');
const User = require('../models/user');
const Tweet = require('../models/tweet');
const Joi = require('joi');

// Renders the admin login page
exports.adminlogin = {

  auth: false,

  handler: (request, reply) => {
    reply.view('adminlogin', { title: 'Login as Admin' });
  },

};

// Recieves admin details from login form and authenticates it
exports.authenticate = {

  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('adminlogin', {
        title: 'Log in error',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },

  handler: function (request, reply) {
    const admin = request.payload;

    Administrator.findOne({ email: admin.email }).then(foundAdmin => {
      if (foundAdmin && foundAdmin.password === admin.password) {
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: admin.email,
        });
        reply.redirect('/adminhome');
      } else {
        reply.redirect('/adminlogin');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

// Renders admin home page and passes in no. of users and tweets
exports.adminhome = {

  handler: function (request, reply) {
    User.find({}).then(allUsers => {
      Tweet.find({}).populate('tweetor').then(allTweets => {
        reply.view('adminpage', {
          title: 'Login as Admin',
          numtweets: allTweets.length,
          numusers: allUsers.length,
        });
      });
    });
  },
};

// Clears cookie and logs out admin
exports.logout = {

  auth: false,

  handler: function (request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/');
  },
};

// Renders page allowing admin to register a user
exports.adminRegister = {

  handler: (request, reply) => {
    reply.view('adminregisteruser', {
      title: 'Admin user registration', });
  },
};

// Recieves user details from sign up form and creates new user
exports.registerUser = {

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    failAction: function (request, reply, source, error) {
      reply.view('adminregisteruser', {
        title: 'Error registering user',
        errors: error.data.details,
      }).code(400);
    },

    options: {
      abortEarly: false,
    },
  },

  handler: function (request, reply) {
    let d = new Date();
    let datestring = ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) +
        '-' + d.getFullYear();
    const user = new User(request.payload);

    user.regdate = datestring;

    user.save().then(newUser => {
      reply.redirect('/adminregister');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Renders adminusers page passing in details of all current users
exports.adminUsers = {

  handler: function (request, reply) {
    User.find({}).then(allUsers => {
      reply.view('adminusers', {
        title: 'All users',
        users: allUsers,
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};

// Deletes a users tweets followed by the user itself
exports.deleteUser = {

  handler: function (request, reply) {
    let userid = request.params.id;

    Tweet.find({ tweetor: userid }).then(userTweets => {
      userTweets.forEach(function (tweetid) {
        Tweet.findByIdAndRemove(tweetid, function (error) {
          if (error) {
            throw error;
          }
        });
      });
    }).then(whatever => {
      User.findOneAndRemove({ _id: userid }, function (error) {
        if (error) {
          reply({
            message: 'Error removing user',
          });
        } else {
          reply.redirect('/adminusers');
        }
      });
    }).catch(err => {
      reply.redirect('/adminusers');
    });
  },
};

// Deletes all of a user's tweets
exports.deleteUserTweets = {

  handler: function (request, reply) {
    let id = request.params.id;

    User.findOne({}).then(user => {
      Tweet.find({ tweetor: user }).then(usersTweets => {
        usersTweets.forEach(function (tweetid) {
          Tweet.findByIdAndRemove(tweetid, function (error) {
            if (error) {
              throw error;
            }
          });
        });

        return null;
      });
    }).then(whatever => {
      reply.redirect('/admintimeline');
    }).catch(err => {
      reply.redirect('/adminhome');
    });
  },
};

// Renders global timeline for admin showing all tweets
exports.adminTimeline = {

  handler: function (request, reply) {
    Tweet.find({}).populate('tweetor').then(allTweets => {
      reply.view('admintimeline', {
        title: 'Admin timeline',
        tweets: allTweets.reverse(),
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Retrieves a tweet's picture based on its id and renders it to timeline
exports.getPicture = {
  auth: false,
  handler: function (request, reply) {
    Tweet.findOne({ _id: request.params.id }).then(tweet => {
      reply(tweet.picture.data).type('image');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Deletes a single tweet
exports.deleteTweet = {

  handler: function (request, reply) {
    let id = request.params.id;

    Tweet.findOneAndRemove({ _id: id }, function (error) {
      if (error) {
        reply({
          statusCode: 503,
          message: 'Error removing tweet',
        });
      } else {
        reply.redirect('/admintimeline');
      }
    });
  },
};
