/**
 * Created by John on 11/10/2016.
 */
'use strict';

// Allows use of images in views
exports.servePublicDirectory = {
  directory: {
    path: 'public',
  },

};
