/**
 * Created by John on 01/11/2016.
 */
'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');
const _ = require('lodash');

// Renders allusers view populated with all registered users minus logged in user
exports.allUsers = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ email: userEmail }).then(user => {
      User.find({}).then(allUsers => {
        let users = [];
        allUsers.forEach(function (userid) {
          if (!userid.equals(user._id)) {
            users.push(userid);
          }
        });

        reply.view('allusers', {
          title: 'All users',
          users: users,
        });
      }).catch(err => {
        reply.redirect('/timeline');
      });
    });
  },
};

// Renders the public timeline of another user
exports.publicTimeline = {

  handler: function (request, reply) {
    let id = request.params.id;

    User.findOne({ _id: id }).then(user => {
      Tweet.find({ tweetor: user }).then(usersTweets => {
        reply.view('otherusertimeline', {
          title: 'Public timeline',
          tweets: usersTweets,
          user: user,
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};

//Follow another user i.e. add that user to list of following in user model
exports.follow = {

  handler: function (request, reply) {
    let id = request.params.id;
    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ _id: id }).then(newFriend => {
      User.findOne({ email: userEmail }).then(foundUser => {
        foundUser.following.push(newFriend._id);
        newFriend.followers.push(foundUser._id);
        foundUser.save();
        newFriend.save();
      }).then(foundUser => {
        reply.redirect('/following');
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};
