/**
 * Created by John on 18/12/2016.
 */
'use strict'

const assert = require('chai').assert;
const MyTweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Users API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;

  //const mytweetService = new MyTweetService('http://localhost:4000');
  const mytweetService = new MyTweetService(fixtures.mytweetService);

  beforeEach(function () {
    mytweetService.deleteAllUsers();
  });

  afterEach(function () {
    mytweetService.deleteAllUsers();
  });

  test('create a user', function () {

    const returnedUser = mytweetService.createUser(newUser);
    assert(_.some([returnedUser], newUser), 'returnedUser must be a superset of newUser');
    assert.isDefined(returnedUser._id);

  });

  test('get user', function () {
    const c1 = mytweetService.createUser(newUser);
    const c2 = mytweetService.getUser(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get invalid user', function () {
    const c1 = mytweetService.getUser('1234');
    assert.isNull(c1);
    const c2 = mytweetService.getUser('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a user', function () {
    const u = mytweetService.createUser(newUser);
    assert(mytweetService.getUser(u._id) != null);
    mytweetService.deleteOneUser(u._id);
    assert(mytweetService.getUser(u._id) == null);
  });

  test('get all users', function () {
    for (let u of users) {
      mytweetService.createUser(u);
    }

    const allUsers = mytweetService.getUsers();
    assert.equal(allUsers.length, users.length);
  });

  test('get users detail', function () {
    for (let u of users) {
      mytweetService.createUser(u);
    }

    const allUsers = mytweetService.getUsers();
    for (var i = 0; i < users.length; i++) {
      assert(_.some([allUsers[i]], users[i]), 'returnedUser must be a superset of newUser');
    }
  });

  test('get all users empty', function () {
    const allUsers = mytweetService.getUsers();
    assert.equal(allUsers.length, 0);
  });

});
