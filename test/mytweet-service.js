/**
 * Created by John on 18/12/2016.
 */
'use strict'

const SyncHttpService = require('./sync-http-service');
const baseUrl = 'http://localhost:4000';

class MyTweetService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getUsers() {
    return this.httpService.get('/api/users');
  }

  getUser(id) {
    return this.httpService.get('/api/users/' + id);
  }

  createUser(newUser) {
    return this.httpService.post('/api/users', newUser);
  }

  deleteOneUser(id) {
    return this.httpService.delete('/api/users/' + id);
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/users');
  }

  login() {
    return this.httpService.get('/api/users/{email}/{password}');
  }

  createTweet(id, tweet) {
    return this.httpService.post('/api/users/' + id + '/tweets', tweet);
  }

  getTweets(id) {
    return this.httpService.get('/api/users/' + id + '/tweets');
  }

  deleteAllTweets() {
    return this.httpService.delete('/api/tweets');
  }

  deleteUsersTweets(id) {
    return this.httpService.delete('/api/users/' + id + '/tweets');
  }

  deleteOneTweet(id) {
    return this.httpService.delete('/api/tweets/' + id);
  }
}

module.exports = MyTweetService;
