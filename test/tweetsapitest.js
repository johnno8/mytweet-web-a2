/**
 * Created by John on 19/12/2016.
 */
'use strict'

const assert = require('chai').assert;
const MyTweetService = require('./mytweet-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweets API tests', function () {

  let users = fixtures.users;
  let newTweet = fixtures.newTweet;
  let newUser = fixtures.newUser;
  let newUser2 = fixtures.newUser2;
  let tweets = fixtures.tweets;

  //const mytweetService = new MyTweetService('http://localhost:4000');
  const mytweetService = new MyTweetService(fixtures.mytweetService);

  beforeEach(function () {
    mytweetService.deleteAllTweets();
    mytweetService.deleteAllUsers();
  });

  afterEach(function () {
    mytweetService.deleteAllTweets();
    mytweetService.deleteAllUsers();
  });

  test('create a tweet', function () {
    const returnedUser = mytweetService.createUser(newUser);
    mytweetService.createTweet(returnedUser._id, tweets[0]);
    const returnedTweets = mytweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, 1);
    assert(_.some([returnedTweets[0]], tweets[0]), 'returnedTweet must be a superset of newTweet');
  });

  test('create multiple tweets', function () {
    const returnedUser = mytweetService.createUser(newUser);
    for (var i = 0; i < tweets.length; i++) {
      mytweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const returnedTweets = mytweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, tweets.length);
    for (var i = 0; i < tweets.length; i++) {
      assert(_.some([returnedTweets[i]], tweets[i]), 'returnedTweet must be a superset of tweet');
    }
  });

  test('delete all tweets', function () {
    const returnedUser = mytweetService.createUser(newUser);
    for (var i = 0; i < tweets.length; i++) {
      mytweetService.createTweet(returnedUser._id, tweets[i]);
    }

    const t1 = mytweetService.getTweets(returnedUser._id);
    assert.equal(t1.length, tweets.length);
    mytweetService.deleteAllTweets();
    const t2 = mytweetService.getTweets(returnedUser._id);
    assert.equal(t2.length, 0);
  });

  test('delete tweets for one user', function () {
    const returnedUser = mytweetService.createUser(newUser);
    const returnedUser2 = mytweetService.createUser(newUser2);

    for (var i = 0; i < tweets.length; i++) {
      mytweetService.createTweet(returnedUser._id, tweets[i]);
    }

    for (var i = 0; i < tweets.length; i++) {
      mytweetService.createTweet(returnedUser2._id, tweets[i]);
    }

    const t1 = mytweetService.getTweets(returnedUser._id);
    const t2 = mytweetService.getTweets(returnedUser2._id);
    assert.equal(t1.length, tweets.length);
    assert.equal(t2.length, tweets.length);

    mytweetService.deleteUsersTweets(returnedUser._id);
    const t3 = mytweetService.getTweets(returnedUser._id);
    assert.equal(t3.length, 0);

    const t4 = mytweetService.getTweets(returnedUser2._id);
    assert.equal(t4.length, tweets.length);
  });

  test('delete one tweet', function () {
    const returnedUser = mytweetService.createUser(newUser);
    mytweetService.createTweet(returnedUser._id, tweets[0]);
    const returnedTweets = mytweetService.getTweets(returnedUser._id);
    assert.equal(returnedTweets.length, 1);

    mytweetService.deleteOneTweet(returnedTweets[0]._id);
    const t1 = mytweetService.getTweets(returnedUser._id);
    assert.equal(t1.length, 0);
  });

});
