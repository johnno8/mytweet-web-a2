# MyTweet

MyTweet is a dynamic microblogging web app developed in [node.js](https://nodejs.org/) using the [hapi.js](https://hapijs.com/)  framework with a [MongoDB](https://www.mongodb.com/) backend provided by [mLab](https://mlab.com/). It allows users to register for an account, log in, make blog posts of text and images, and see other user's posts.

### Quick Start
The app is hosted on [Heroku](https://www.heroku.com/) at: [https://boiling-lake-56131.herokuapp.com](https://boiling-lake-56131.herokuapp.com)
##### Valid Demo Logins
      USER
        email: homer@simpson.com
        password: secret
        
      ADMIN
        email: admin@mytweet.ie
        password: secret

### API
MyTweet provides an API with the following endpoints:

  - Get all Users   
    method: 'GET'   
    path: '/api/users'   
    Returns all registered Users in JSON format

  - Get one User   
    method: 'GET'   
    path: '/api/users/{id} - where id is a valid user id'   
    Returns a single specified User in JSON format

  - Create a new user   
    method: 'POST'   
    path: '/api/users'   
    Returns a new User object

  - Delete one User   
    method: 'DELETE'    
    path: '/api/users/{id}' - where id is a valid User id   
    Returns a success code or an error

  - Delete all Users   
    method: 'DELETE'   
    path: '/api/users'   
    Returns a success code or an error

  - Log in a User   
    method: 'GET'   
    path: '/api/users/{email}/{password}' - where email and password are User's login details   
    Checks Users details against hashed and salted details and returns success code or error

  - Get all tweets   
    method: 'GET'   
    path: '/api/tweets'   
    Returns  list of all tweets

  - Get single User's tweets   
    method: 'GET'   
    path: '/api/users/{id}/tweets' - where id is a valid User id   
    Returns a list of specified User's tweets

  - Send a tweet   
    method: 'POST'   
    path: '/api/users/{id}/tweets' - where id is a valid User id   
    Creates and posts a new tweet

  - Delete all tweets   
    method: 'DELETE'   
    path: '/api/tweets'   
    Deletes all tweets and returns a success code or an error

  - Delete one User's tweets   
    method: 'DELETE'   
    path: '/api/users/{id}/tweets' - where id is a valid User id   
    Deletes all tweets for the specified User and returns a success code or an error

  - Delete one tweet   
    method: 'DELETE'   
    path: '/api/tweets/{id}' - where id is a tweet id   
    Deletes specified tweet and returns a success code or an error

### Known Bugs

  - The Users page shows a list of all users, minus the current user, but followed users still appear with the option of following them again
  - A User can follow another User multiple times
  - In testing on localhost the tweets of followed Users were rendered in the Following page, however on deployment to Heroku and AWS this functionality ceased. The bug was fixed and worked once upon redeployment to Heroku but subsequently failed again